# FalconApp

This class creates an instance of the Falcon ASGI application.

It sets up the media and error handlers for the application.


### Media Handlers

Custom JSON handler for the application to handles HTTP Content-Type `application/json` and `application/problems+json`.

- Uses the `python-rapidjson` library for faster JSON encoding and decoding.<br>
- Uses the ISO 8601 datetime format and UUID canonical format.

### RFC 9457 Errors

Implement [RFC 9457](https://www.rfc-editor.org/rfc/rfc9457) compliant error response for Python and Falcon HTTP errors.<br>
Optional implementation is available for Pydantic Validation errors.


## Instance

Property to get the current instance of the Falcon ASGI application.

```python
from fatils import falcon_app

app = falcon_app.instance
```

## Create a new instance

Creates and returns a new instance of the Falcon application.

It sets up the media and error handlers for the application.

```python
from fatils import falcon_app
from fatils.app import ValidatorEnum

app = falcon_app.create_instance()
# Optionally:
app = falcon_app.create_instance(validator=ValidatorEnum.PYDANTIC)
```
