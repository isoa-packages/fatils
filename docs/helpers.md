# Helpers

# Logging

Uses [Loguru](https://github.com/Delgan/loguru)

## Tests

Uses `contextmanager` for tests.<br>
Remove any other existing logger..<br>

```python
from fatils import capture_logs


def test_add_function():
    with capture_logs() as logs:
        add_function()
    self.assertEqual(len(logs), 1)
    self.assertEqual(logs[0].get("level"), "INFO")
    self.assertEqual(logs[0].get("message"), "Useless function")

```

<br>

# Mongo Tests

Provides a basic class for Mongo tests. Inherit unittest async test cases.<br>
Simulate the calls Falcon middlewares would do at startup and shutdwon.<br>

```python
from fatils import BaseMongoTest


class CustomTest(BaseMongoTest):
    async def asyncSetUp(self):
        mongo_impl = MongoImpl()
        fixtures_impl = FixturesImpl()
        await super().asyncSetUp(
            mongo=mongo_impl,
            fixtures=fixtures_impl
        )
        # fixtures is optional.
        # If not provided, only mongo middleware will be used with provided implementation.
```

<br>

# Datetime

Provides functions related to datetime.

```python
from fatils import UTC
from fatils import iso_utc_datetime
from fatils import utc_datetime

# `UTC` is a ZoneInfo object set with IANA UTC TimeZone.

# Current UTC date and time (datetime type)
utc_now = utc_datetime()
assert utc_now.tzinfo == UTC

# Current UTC date and time in ISO format (str type)
iso_utc_now = iso_utc_datetime()
# > 2015-04-25T06:30:15.123456+01:00
```
