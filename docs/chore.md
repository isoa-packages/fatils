# Requirements

Use `requirements/all.txt` for dev requirements (will install everything..)<br>
Dev / Lint / CI Dependencies are under `requirements/dev.in` / `requirements/dev.txt` <br>
Required package dependencies are under `requirements/project.in` / `requirements/project.txt` <br>

Pre-commit hooks are also needed. <br>

- `virtualenv .venv`
- `source .venv/bin/activate`
- `pip install -r requirements/all.txt`
- `pre-commit install`
- `pre-commit install --hook-type commit-msg`

## Dependency management

This project uses [pip-compile-multi](https://pypi.org/project/pip-compile-multi/) for hard-pinning dependencies versions.
Please see its documentation for usage instructions.
In short, `requirements/project.in` contains the list of direct requirements with occasional version constraints (like `Django<2`)
and `requirements/project.txt` is automatically generated from it by adding recursive tree of dependencies with fixed versions.
The same goes for `dev`.

To upgrade dependency versions, run `pip-compile-multi`.

To add a new dependency without upgrade, add it to `requirements/project.in` and run `pip-compile-multi --no-upgrade`.

For installation always use `.txt` files. For example, command `pip install -Ue . -r requirements/dev.txt` will install
this project in development mode, testing requirements and development tools.
Another useful command is `pip-sync requirements/dev.txt`, it uninstalls packages from your virtualenv that aren't listed in the file.

<br>

# Testing

This project uses the built-in [unittest package](https://docs.python.org/3/library/unittest.html).<br>
The coverage is handled by [Coverage](https://coverage.readthedocs.io/en/latest/)<br>

To test the package, follow the [Requirements](#requirements) section and use coverage (or unittest alone) to run the tests:

- `coverage run -m unittest`
- `coverage report -m`
- `coverage html`
