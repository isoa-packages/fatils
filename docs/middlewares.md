# Middlewares

# Mongo

Middleware used to connect and disconnect to a Mongo Database.<br>
It has to be initiated with an implementation of [`MongoInterface`](../fatils/interfaces/mongo.py)<br>

```python
from fatils import MongoMiddleware

falcon_app.add_middleware(MongoMiddleware(mongo_impl))
```

<br>

# Fixtures

Middleware used to insert and delete fixtures into the database.<br>
It has to be initiated with an implementation of [`FixturesInterface`](../fatils/interfaces/fixtures.py)<br>

```python
from fatils import FixturesMiddleware

falcon_app.add_middleware(FixturesMiddleware(fixtures_impl))
```
