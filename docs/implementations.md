# Implementations

# MongoBeanie

Implementation used to connect and disconnect from MongoDB.<br>
Expected by [`MongoMiddleware`](../fatils/middlewares/mongo.py)<br>

```python
from fatils import MongoBeanie
from fatils import MongoMiddleware

mongo_beanie = MongoBeanie(beanie_documents, secret_uri, database_name)

falcon_app.add_middleware(MongoMiddleware(mongo_beanie))
```

<br>

# FixturesBeanie

Implementation used to insert and delete fixtures into the database.<br>
Expected by [`FixturesMiddleware`](../fatils/middlewares/fixtures.py)<br>

```python
from fatils import DatabaseFixtures
from fatils import FixturesBeanie
from fatils import BeanieDocumentFixture

fixtures_beanie = FixturesBeanie([
    BeanieDocumentFixture('path/one.json', BeanieDocumentOne),
    BeanieDocumentFixture('path/two.json', BeanieDocumentTwo),
    BeanieDocumentFixture('path/diff/three.json', BeanieDocumentThree, custom_insert_func),
])

falcon_app.add_middleware(FixturesMiddleware(fixtures_beanie))
```
