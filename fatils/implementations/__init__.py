__all__ = []


# -----------------------------------------------------------------
# Beanie ODM Implementations
# -----------------------------------------------------------------
try:
    from .fixtures_beanie import BeanieDocumentFixture
    from .fixtures_beanie import FixturesBeanie
    from .mongo_beanie import MongoBeanie

    __all__ += [
        "BeanieDocumentFixture",
        "FixturesBeanie",
        "MongoBeanie",
    ]
except ImportError as error:  # pragma: no cover
    raise ImportError("missing dependencies, install `beanie`") from error
