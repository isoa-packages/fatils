from beanie import Document
from beanie import init_beanie
from motor.motor_asyncio import AsyncIOMotorClient


try:
    # Pydantic should be installed by beanie, but just in case...
    from pydantic import SecretStr
except ImportError as error:  # pragma: no cover
    raise ImportError("missing dependencies, install `pydantic`") from error


class MongoBeanie:
    """
    A class for interacting with MongoDB using the Beanie library.

    Provides `connect` and `disconnect` methods.

    Args:
        documents (list[Document]): A list of Beanie Document classes to be used with the database.
        secret_uri (SecretStr): A SecretStr object containing the MongoDB connection URI.
        database_name (str): The name of the MongoDB database to be used.
    """

    def __init__(
        self,
        documents: list[Document],
        secret_uri: SecretStr,
        database_name: str,
    ) -> None:
        self._client = None
        self.documents = documents
        self.secret_uri = secret_uri
        self.database_name = database_name

    async def connect(self) -> None:
        """
        Connects to the MongoDB database using the provided connection URI.
        """
        self._client = AsyncIOMotorClient(self.secret_uri.get_secret_value())

        # Initialize beanie with given database and collections
        await init_beanie(
            database=self._client.get_database(self.database_name),
            document_models=self.documents,
        )

    async def disconnect(self) -> None:
        """
        Disconnects from the MongoDB database.
        """
        if self._client is not None:
            self._client.close()
            self._client = None
