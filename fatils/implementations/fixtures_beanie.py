from dataclasses import dataclass
from dataclasses import field
from typing import Callable

from beanie import Document
from rapidjson import DM_ISO8601
from rapidjson import UM_CANONICAL
from rapidjson import load


try:
    # Pydantic should be installed by beanie, but just in case...
    from pydantic.types import FilePath
except ImportError as error:  # pragma: no cover
    raise ImportError("missing dependencies, install `pydantic`") from error


async def _insert_function(path: FilePath, document: Document) -> None:
    """
    This function is used to insert data from a JSON file into a Beanie document.

    Args:
        path (FilePath): The path to the JSON file.
        document (Document): The Beanie document class.
    """
    with open(path) as json_file:
        json = load(
            json_file,
            datetime_mode=DM_ISO8601,
            uuid_mode=UM_CANONICAL,
        )
        documents = [document(**entry) for entry in json]
        await document.insert_many(documents)


@dataclass
class BeanieDocumentFixture:
    """
    Represents a single Beanie document fixture.

    Args:
        path (FilePath): The path to the JSON file containing the data to be inserted into the database.
        document (Document): The Beanie document class that will be used to insert the data.
        insert_function (Callable, optional): A function that will be used to insert the data into the database.
            The default value is `_insert_function`.
    """

    path: FilePath
    document: Document
    insert_function: Callable = field(default=_insert_function)


class FixturesBeanie:
    """
    Represents a collection of Beanie documents to be used in a database as test data.

    Provides `insert` and `delete` methods.

    Args:
        fixtures (list[BeanieDocumentFixture]): A list of BeanieDocumentFixture objects.
    """

    def __init__(self, fixtures: list[BeanieDocumentFixture]) -> None:
        self.fixtures = fixtures

    async def insert(self) -> None:
        """
        Insert fixtures into the database.

        This method loops through each fixture in the list and calls the insert_function with the appropriate arguments.
        The insert_function is a user-defined function that can be customized to meet the specific needs of the application.
        By default, the insert_function is set to a default implementation that can handle most use cases.
        """
        for fixture in self.fixtures:
            await fixture.insert_function(fixture.path, fixture.document)

    async def delete(self) -> None:
        """
        Delete all fixtures from the database.

        This method loops through each fixture in the list and calls the delete_all method on the associated BeanieDocument object.
        """
        for fixture in self.fixtures:
            await fixture.document.delete_all()
