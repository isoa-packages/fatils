from typing import Optional

from loguru import logger

from fatils.interfaces import FixturesInterface


class FixturesMiddleware:
    """
    ASGI middleware class that handles the insertion and deletion of fixtures data on startup and shutdown of the Falcon application.

    Args:
        fixtures (FixturesInterface): An interface for interacting with the fixtures data.
        clean_on_startup (Optional[bool], optional): A flag indicating whether to clean the fixtures data before starting. Defaults to True.
    """

    def __init__(
        self,
        fixtures: FixturesInterface,
        clean_on_startup: Optional[bool] = True,
    ) -> None:
        self.fixtures = fixtures
        self.clean_on_startup = clean_on_startup

    async def process_startup(self, scope: dict, event: dict) -> None:
        """
        A method that is called when the Falcon application starts. It inserts the fixtures data into the database.
        Optionally deletes the fixtures data from the database before starting if `clean_on_startup` is True.

        Args:
            scope (dict): The ASGI scope.
            event (dict): The ASGI event.
        """
        if self.clean_on_startup:
            try:
                await self.fixtures.delete()
            except Exception as error:
                logger.exception("Failed to delete fixtures")
                logger.exception(error)
                return
            else:
                logger.success("Fixtures deleted")

        try:
            await self.fixtures.insert()
        except Exception as error:
            logger.exception("Failed to insert fixtures")
            logger.exception(error)
            return
        else:
            logger.success("Fixtures inserted")

    async def process_shutdown(self, scope: dict, event: dict) -> None:
        """
        A method that is called when the Falcon application shuts down. It deletes the fixtures data from the database.

        Args:
            scope (dict): The ASGI scope.
            event (dict): The ASGI event.
        """
        try:
            await self.fixtures.delete()
        except Exception as error:
            logger.exception("Failed to delete fixtures")
            logger.exception(error)
            return
        else:
            logger.success("Fixtures deleted")
