from loguru import logger

from fatils.interfaces import MongoInterface


class MongoMiddleware:
    """
    ASGI middleware class that handles the connection and disconnection to MongoDB on startup and shutdown of the Falcon application.

    Args:
        mongo (MongoInterface): The MongoDB interface to use for connecting and disconnecting.
    """

    def __init__(self, mongo: MongoInterface) -> None:
        self.mongo = mongo

    async def process_startup(self, scope: dict, event: dict) -> None:
        """
        A method that is called when the Falcon application starts. It connect to MongoDB on startup.

        Args:
            scope (dict): The ASGI scope.
            event (dict): The ASGI event.
        """
        try:
            await self.mongo.connect()
        except Exception as error:
            logger.exception("Failed connection to MongoDB")
            logger.exception(error)
        else:
            logger.success("Connected to MongoDB")

    async def process_shutdown(self, scope: dict, event: dict) -> None:
        """
        A method that is called when the Falcon application shuts down. It disconnect from MongoDB on shutdown.

        Args:
            scope (dict): The ASGI scope.
            event (dict): The ASGI event.
        """
        try:
            await self.mongo.disconnect()
        except Exception as error:
            logger.exception("Failed disconnection from MongoDB")
            logger.exception(error)
        else:
            logger.success("Disconnected from MongoDB")
