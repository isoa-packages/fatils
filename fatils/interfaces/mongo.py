from typing import Protocol


class MongoInterface(Protocol):
    """
    Interface for interacting with a MongoDB database.
    """

    async def connect(self) -> None:
        """Handle connection to the database"""

        ...  # pragma: no cover

    async def disconnect(self) -> None:
        """Handle disconnection from the database."""
        ...  # pragma: no cover
