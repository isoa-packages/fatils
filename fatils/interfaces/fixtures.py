from typing import Protocol


class FixturesInterface(Protocol):
    """
    Interface for interacting with test data.
    """

    async def insert(self) -> None:
        """Handle insertion of test data"""
        ...  # pragma: no cover

    async def delete(self) -> None:
        """Handle deletion of test data"""
        ...  # pragma: no cover
