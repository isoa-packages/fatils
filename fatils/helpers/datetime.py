from datetime import datetime
from zoneinfo import ZoneInfo


IANA_UTC_TIMEZONE = "Etc/UTC"

UTC = ZoneInfo(IANA_UTC_TIMEZONE)


def utc_datetime() -> datetime:
    """
    Get the current UTC date and time.

    Returns:
        datetime: The current UTC date and time.
    """
    return datetime.now(UTC)


def iso_utc_datetime() -> str:
    """
    Get the current UTC date and time in ISO format.

    Returns:
        str: The current UTC date and time in ISO format.
    """
    return utc_datetime().isoformat()
