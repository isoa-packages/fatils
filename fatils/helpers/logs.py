from contextlib import contextmanager
from contextlib import suppress
from typing import Any
from typing import Generator
from typing import Optional

from loguru import logger


DEFAULT_LOG_FORMAT = "<level>{time} | {level: <8} | {file}:{function}:{line} | {message}</level>"
TEST_LOG_FORMAT = "{level} | {message}"


@contextmanager
def capture_logs(
    level: Optional[str] = "DEBUG",
    format: Optional[str] = TEST_LOG_FORMAT,
) -> Generator[list[dict[str, Any]], None, None]:
    """
    Context manager to capture all Loguru based logs emitted during its context.

    Args:
        level (Optional[str], optional): Minimum log level to capture. Defaults to "DEBUG".
        format (Optional[str], optional): Log format. Defaults to TEST_LOG_FORMAT.

    Yields:
        Generator[list[dict[str, Any]], None, None]: Captured logs.
    """
    logger.remove()  # Remove all loggers

    output = []

    def sink(log):
        output.append(
            {
                "message": log.record["message"],
                "level": log.record["level"].name,
            }
        )

    handler_id = logger.add(sink, level=level, format=format, serialize=True)

    yield output

    with suppress(ValueError):
        logger.remove(handler_id)
