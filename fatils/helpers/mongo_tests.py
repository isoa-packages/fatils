from typing import Any
from typing import Coroutine
from typing import Optional
from unittest import IsolatedAsyncioTestCase

from fatils.interfaces import FixturesInterface
from fatils.interfaces import MongoInterface
from fatils.middlewares import FixturesMiddleware
from fatils.middlewares import MongoMiddleware


class BaseMongoTest(IsolatedAsyncioTestCase):
    """
    Base class for all tests that require a MongoDB connection.

    This class asynchronously sets up and tears down a MongoDB
    connection for each test, optionally handles fixtures.

    Args:
        mongo (MongoInterface): The MongoDB connection to use for the tests.
        fixtures (Optional[FixturesInterface]): Fixtures for loading test
            data. If not provided, no test data will be loaded.
    """

    maxDiff = None

    async def asyncSetUp(
        self,
        mongo: MongoInterface,
        fixtures: Optional[FixturesInterface] = None,
    ) -> Coroutine[Any, Any, None]:
        """
        Set up the test environment.

        This method sets up the MongoDB connection and, if provided, the test
        data fixtures. It also sets up the IsolatedAsyncioTestCase
        infrastructure.

        Args:
            mongo (MongoInterface): The MongoDB connection to use for the tests.
            fixtures (Optional[FixturesInterface]): Fixtures for loading test
                data. If not provided, no test data will be loaded.
        """
        await super().asyncSetUp()
        self._has_fixtures = True if fixtures is not None else False

        self._mongo = MongoMiddleware(mongo)
        await self._mongo.process_startup({}, {})

        if self._has_fixtures:
            self._fixtures = FixturesMiddleware(fixtures)
            await self._fixtures.process_startup({}, {})

    async def asyncTearDown(self) -> Coroutine[Any, Any, None]:
        """
        Tear down the test environment.

        This method tears down the MongoDB connection and, if provided, the
        test data fixtures. It also tears down the IsolatedAsyncioTestCase
        infrastructure.
        """
        await self._mongo.process_shutdown({}, {})

        if self._has_fixtures:
            await self._fixtures.process_shutdown({}, {})

        await super().asyncTearDown()
