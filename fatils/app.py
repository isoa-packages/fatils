from functools import partial
import os

import falcon
from falcon import HTTP_422
from falcon import HTTP_500
from falcon import HTTPError
from falcon import asgi
from falcon import media
from falcon.asgi.request import Request
from falcon.asgi.response import Response
from loguru import logger
from rapidjson import DM_ISO8601
from rapidjson import UM_CANONICAL
from rapidjson import dumps
from rapidjson import loads


MEDIA_PROBLEM = "application/problem+json"
DETAIL_PYDANTIC_ERROR = "The content of your request is invalid."
DETAIL_PYTHON_ERROR = "An unexpected error occurred."


class ValidatorEnum:
    NONE = None
    PYDANTIC = "pydantic"


class _FalconApp:
    """
    This class creates a new instance of the Falcon ASGI application.

    It sets up the media and error handlers for the application.
    """

    def __init__(self):
        self.app = self.create_instance()

    @property
    def instance(self) -> asgi.App:
        """
        This method returns the Falcon ASGI application instance.

        Returns:
            asgi.App: The Falcon ASGI application instance.

        """
        return self.app

    async def _rfc_pydantic_error(
        self, req: Request, resp: Response, ex: HTTPError, params: dict
    ) -> None:
        """
        This method handles Pydantic validation error.

        It returns a RFC 9457 Compliant Error response.

        Args:
            req (Request): The Falcon request object.
            resp (Response): The Falcon response object.
            ex (HTTPError): The Pydantic validation error.
            params (dict): Not used.

        """
        # https://docs.pydantic.dev/latest/usage/models/#error-handling

        rfc = {
            "type": "about:blank",
            "title": HTTP_422,
            "status": HTTP_422,
            "detail": DETAIL_PYDANTIC_ERROR,
            "instance": req.uri,
            "errors": ex.errors(),
        }

        logger.warning(rfc)

        resp.content_type = MEDIA_PROBLEM
        resp.status = HTTP_422
        resp.media = rfc

    async def _rfc_python_error(
        self, req: Request, resp: Response, ex: Exception, params: dict
    ) -> None:
        """
        This method handles Python error.

        It returns a RFC 9457 Compliant Error response.

        Args:
            req (Request): The Falcon request object.
            resp (Response): The Falcon response object.
            ex (HTTPError): The Falcon HTTP error.
            params (dict): Not used.

        """
        exc_next = ex.__traceback__.tb_next
        exc_next_code = exc_next.tb_frame.f_code

        exc_type = type(ex).__name__
        exc_file = os.path.basename(exc_next_code.co_filename)
        exc_func = exc_next_code.co_name
        exc_line = exc_next.tb_lineno

        rfc = {
            "type": "about:blank",
            "title": HTTP_500,
            "status": HTTP_500,
            "detail": DETAIL_PYTHON_ERROR,
            "instance": req.uri,
            "debug": {
                "type": exc_type,
                "file": exc_file,
                "func": exc_func,
                "line": exc_line,
            },
        }

        logger.warning(rfc)

        resp.content_type = MEDIA_PROBLEM
        resp.status = HTTP_500
        resp.media = rfc

    async def _rfc_http_error(
        self, req: Request, resp: Response, ex: HTTPError, params: dict
    ) -> None:
        """
        This method handles Falcon HTTP error.

        It returns a RFC 9457 Compliant Error response.

        Args:
            req (Request): The Falcon request object.
            resp (Response): The Falcon response object.
            ex (HTTPError): The Falcon HTTP error.
            params (dict): Not used.

        """

        rfc = {
            "type": "about:blank",
            "title": ex.status,
            "status": ex.status,
            "detail": ex.description,
            "instance": req.uri,
        }

        logger.warning(rfc)

        resp.content_type = MEDIA_PROBLEM
        resp.status = ex.status
        resp.media = rfc

    def _json_handler(self) -> media.JSONHandler:
        """
        This method creates a JSON handler for the application.

        It uses the python-rapidjson library for faster JSON encoding and decoding.
        It uses the ISO 8601 datetime format and UUID canonical format.

        Returns:
            media.JSONHandler: The Falcon JSON handler instance.

        """
        return media.JSONHandler(
            dumps=partial(
                dumps,
                datetime_mode=DM_ISO8601,
                uuid_mode=UM_CANONICAL,
            ),
            loads=partial(
                loads,
                datetime_mode=DM_ISO8601,
                uuid_mode=UM_CANONICAL,
            ),
        )

    def _add_media_handlers(self) -> None:
        """
        This method adds media handlers to the application.

        It adds support for JSON:API and PROBLEM+JSON:API media types and sets up the JSON handler.

        """
        json_handler = self._json_handler()

        handlers = {
            MEDIA_PROBLEM: json_handler,
            falcon.MEDIA_JSON: json_handler,
        }

        self.app.req_options.media_handlers.update(handlers)
        self.app.resp_options.media_handlers.update(handlers)

    def create_instance(
        self, validator: ValidatorEnum = ValidatorEnum.NONE
    ) -> asgi.App:
        """
        This method creates a new instance of the Falcon ASGI application.

        It sets up the media and error handlers for the application.

        Returns:
            asgi.App: The Falcon ASGI application instance.

        """
        self.app = asgi.App()

        self._add_media_handlers()

        if validator is ValidatorEnum.PYDANTIC:
            try:
                from pydantic import ValidationError
            except ImportError as error:  # pragma: no cover
                raise ImportError(
                    "missing dependencies, install `pydantic`"
                ) from error
            self.app.add_error_handler(
                ValidationError, self._rfc_pydantic_error
            )

        self.app.add_error_handler(HTTPError, self._rfc_http_error)
        self.app.add_error_handler(Exception, self._rfc_python_error)

        return self.app


falcon_app = _FalconApp()
