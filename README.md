# FATILS

#### Falcon Utilities

Package containing utilities such as middlewares, base tests classes or logs handler for Falcon.<br>
Intended to be used accros multiple ISOA projects.

<br>

# Install

You can install this package using this project's PyPi registry:

```
pip install fatils --index-url https://gitlab.com/api/v4/groups/81553334/-/packages/pypi/simple
```

> By default, only `falcon`, `python-rapidjson` and `loguru` is installed with this package.<br>
> Other features are available if your project has `Pydantic` or `Beanie ODM` libraries installed
<!-- > Anything else will require optional dependencies. -->

Optional features:
- Using `Pydantic`: RFC 9457 Error message on validation of data
- Using `Beanie ODM`: Implementation of MongoDB and Fixtures middlewares

<br>

# Documentation

Check out the [docs](/docs/all.md) for more informations.
