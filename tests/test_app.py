from copy import deepcopy
from datetime import datetime
from typing import Any
from unittest import IsolatedAsyncioTestCase
from uuid import UUID

from falcon import HTTP_200
from falcon import HTTP_418
from falcon import HTTP_422
from falcon import HTTP_500
from falcon import HTTPError
from falcon import asgi
from falcon import testing
from falcon.asgi import Request
from falcon.asgi import Response
from pydantic import UUID4
from pydantic import AwareDatetime
from pydantic import BaseModel
from pydantic import Field
from pydantic import ValidationError

from fatils import capture_logs
from fatils import falcon_app
from fatils.app import DETAIL_PYDANTIC_ERROR
from fatils.app import ValidatorEnum


class Model(BaseModel):
    int: int
    uuid: UUID4 = Field(default=UUID("dc448e91-0b71-4ced-ba4d-2520ec60f093"))
    datetime: AwareDatetime = Field(
        default=datetime.fromisoformat("2024-02-21T18:30:00.000000")
    )


class FakeRoute:
    async def on_get_http(self, req: Request, resp: Response):
        raise HTTPError(HTTP_418, description="Might wanna try coffee")

    async def on_get_python(self, req: Request, resp: Response):
        50 / 0

    async def on_get_pydantic(self, req: Request, resp: Response, num: Any):
        model = Model(int=num)
        resp.media = model.model_dump()


class TestFalconApp(testing.TestCase, IsolatedAsyncioTestCase):
    maxDiff = None

    def setUp(self) -> None:
        super().setUp()
        self.app = falcon_app.create_instance(validator=ValidatorEnum.PYDANTIC)

        self.app.add_route("/http", FakeRoute(), suffix="http")
        self.app.add_route("/python", FakeRoute(), suffix="python")
        self.app.add_route("/pydantic/{num}", FakeRoute(), suffix="pydantic")

    def test_create_instance_no_validator(self):
        new_app = falcon_app.create_instance()

        self.assertNotEqual(self.app, new_app)
        self.assertIsInstance(self.app, asgi.App)
        self.assertIsInstance(new_app, asgi.App)
        self.assertIsNone(new_app._error_handlers.get(ValidationError))

    def test_create_instance_validator_pydantic(self):
        new_app = falcon_app.create_instance(validator=ValidatorEnum.PYDANTIC)

        self.assertNotEqual(self.app, new_app)
        self.assertIsInstance(self.app, asgi.App)
        self.assertIsInstance(new_app, asgi.App)
        self.assertIsNotNone(new_app._error_handlers.get(ValidationError))

    def test_instance(self):
        curr_app = falcon_app.instance

        self.assertEqual(self.app, curr_app)
        self.assertIsInstance(self.app, asgi.App)
        self.assertIsInstance(curr_app, asgi.App)

    def test_json_handler(self):
        """Should use custom JSON (de)serializer, handling UUID and Datetime data"""
        with capture_logs() as logs:
            response = self.simulate_get("/pydantic/123")

        expected_resp = {
            "int": 123,
            "uuid": "dc448e91-0b71-4ced-ba4d-2520ec60f093",
            "datetime": "2024-02-21T18:30:00",
        }

        self.assertEqual(len(logs), 0)

        self.assertEqual(response.status, HTTP_200)
        self.assertEqual(response.json, expected_resp)

    def test_rfc_http_error(self):
        """Should use custom JSON (de)serializer, reply an RFC 9457 HTTP error and log it"""
        with capture_logs() as logs:
            response = self.simulate_get("/http")

        expected_resp = {
            "type": "about:blank",
            "title": HTTP_418,
            "status": HTTP_418,
            "detail": "Might wanna try coffee",
            "instance": "http://falconframework.org/http",
        }

        self.assertEqual(len(logs), 1)
        self.assertEqual(logs[0].get("level"), "WARNING")
        self.assertEqual(logs[0].get("message"), str(expected_resp))
        self.assertEqual(
            response.headers["Content-Type"], "application/problem+json"
        )
        self.assertEqual(
            response.headers["content-type"], "application/problem+json"
        )

        self.assertEqual(response.status, HTTP_418)
        self.assertEqual(response.json, expected_resp)

    def test_rfc_python_error(self):
        """Should use custom JSON (de)serializer, reply an RFC 9457 Python error and log it"""
        with capture_logs() as logs:
            response = self.simulate_get("/python")

        expected_resp = {
            "type": "about:blank",
            "title": HTTP_500,
            "status": HTTP_500,
            "detail": "An unexpected error occurred.",
            "instance": "http://falconframework.org/python",
            "debug": {
                "type": "ZeroDivisionError",
                "file": "test_app.py",
                "func": "on_get_python",
                "line": 41,
            },
        }

        self.assertEqual(len(logs), 1)
        self.assertEqual(logs[0].get("level"), "WARNING")
        self.assertEqual(logs[0].get("message"), str(expected_resp))
        self.assertEqual(logs[0].get("message"), str(expected_resp))
        self.assertEqual(
            response.headers["Content-Type"], "application/problem+json"
        )
        self.assertEqual(
            response.headers["content-type"], "application/problem+json"
        )

        self.assertEqual(response.status, HTTP_500)
        self.assertEqual(response.json, expected_resp)

    def test_rfc_pydantic_error(self):
        """Should use custom JSON (de)serializer, reply an RFC 9457 Pydantic error and log it"""
        with capture_logs() as logs:
            response = self.simulate_get("/pydantic/will_raise_error")

        expected_resp = {
            "type": "about:blank",
            "title": HTTP_422,
            "status": HTTP_422,
            "detail": DETAIL_PYDANTIC_ERROR,
            "instance": "http://falconframework.org/pydantic/will_raise_error",
            "errors": [
                {
                    "type": "int_parsing",
                    "loc": ["int"],
                    "msg": "Input should be a valid integer, unable to parse string as an integer",
                    "input": "will_raise_error",
                    "url": "https://errors.pydantic.dev/2.7/v/int_parsing",
                }
            ],
        }

        expected_log = deepcopy(expected_resp)
        expected_log["errors"][0]["loc"] = ("int",)

        self.assertEqual(len(logs), 1)
        self.assertEqual(logs[0].get("level"), "WARNING")
        self.assertEqual(logs[0].get("message"), str(expected_log))

        self.assertEqual(response.status, HTTP_422)
        self.assertEqual(response.json, expected_resp)
