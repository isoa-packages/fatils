from unittest import IsolatedAsyncioTestCase
from unittest import mock

from fatils import FixturesMiddleware
from fatils import capture_logs


class FakeFixturesImpl:
    async def insert(self): ...  # pragma: no cover

    async def delete(self): ...  # pragma: no cover


class TestFixturesMiddleware(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.fake_fixtures_impl = FakeFixturesImpl()
        self._db_fixtures = FixturesMiddleware(self.fake_fixtures_impl)

    def assert_log(self, log, level, message):
        self.assertEqual(level, log.get("level"))
        self.assertEqual(message, log.get("message"))

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_success_startup(self, mock_insert, mock_delete):
        """Should call delete then call insert and log both success"""

        with capture_logs() as logs:
            await self._db_fixtures.process_startup({}, {})

        self.assertEqual(len(logs), 2)
        self.assert_log(logs[0], "SUCCESS", "Fixtures deleted")
        self.assert_log(logs[1], "SUCCESS", "Fixtures inserted")

        mock_insert.assert_awaited_once()
        mock_delete.assert_awaited_once()

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_success_startup_without_clean(
        self, mock_insert, mock_delete
    ):
        """Should call insert only and log success"""

        _db_fixtures = FixturesMiddleware(self.fake_fixtures_impl, False)

        with capture_logs() as logs:
            await _db_fixtures.process_startup({}, {})

        self.assertEqual(len(logs), 1)
        self.assert_log(logs[0], "SUCCESS", "Fixtures inserted")

        mock_insert.assert_awaited_once()
        mock_delete.assert_not_awaited()

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_success_shutdown(self, mock_insert, mock_delete):
        """Should call delete and log success"""

        with capture_logs() as logs:
            await self._db_fixtures.process_shutdown({}, {})

        self.assertEqual(len(logs), 1)
        self.assert_log(logs[0], "SUCCESS", "Fixtures deleted")

        mock_insert.assert_not_awaited()
        mock_delete.assert_awaited_once()

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_delete_fail_on_shutdown(self, mock_insert, mock_delete):
        """Should call delete, raise an error and log it"""
        mock_delete.side_effect = Exception("Actual error")

        with capture_logs() as logs:
            await self._db_fixtures.process_shutdown({}, {})

        self.assertEqual(len(logs), 2)
        self.assert_log(logs[0], "ERROR", "Failed to delete fixtures")
        self.assert_log(logs[1], "ERROR", "Actual error")

        mock_insert.assert_not_awaited()
        mock_delete.assert_awaited_once()

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_insert_fail_on_startup(self, mock_insert, mock_delete):
        """Should call delete, raise an error on insert and log all of it"""
        mock_insert.side_effect = Exception("Actual error")

        with capture_logs() as logs:
            await self._db_fixtures.process_startup({}, {})

        self.assertEqual(len(logs), 3)
        self.assert_log(logs[0], "SUCCESS", "Fixtures deleted")
        self.assert_log(logs[1], "ERROR", "Failed to insert fixtures")
        self.assert_log(logs[2], "ERROR", "Actual error")

        mock_insert.assert_awaited_once()
        mock_delete.assert_awaited_once()

    @mock.patch.object(FakeFixturesImpl, "delete")
    @mock.patch.object(FakeFixturesImpl, "insert")
    async def test_delete_fail_on_startup(self, mock_insert, mock_delete):
        """Should call delete, raise an error, log it and not call insert"""
        mock_delete.side_effect = Exception("Actual error")

        with capture_logs() as logs:
            await self._db_fixtures.process_startup({}, {})

        self.assertEqual(len(logs), 2)
        self.assert_log(logs[0], "ERROR", "Failed to delete fixtures")
        self.assert_log(logs[1], "ERROR", "Actual error")

        mock_insert.assert_not_awaited()
        mock_delete.assert_awaited_once()
