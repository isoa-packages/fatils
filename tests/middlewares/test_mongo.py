from unittest import IsolatedAsyncioTestCase
from unittest import mock

from fatils import MongoMiddleware
from fatils import capture_logs


class FakeMongoImpl:
    async def connect(self): ...  # pragma: no cover

    async def disconnect(self): ...  # pragma: no cover


class TestMongoDB(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        fake_mongo_impl = FakeMongoImpl()
        self._mongo = MongoMiddleware(fake_mongo_impl)

    def assert_log(self, log, level, message):
        self.assertEqual(level, log.get("level"))
        self.assertEqual(message, log.get("message"))

    @mock.patch.object(FakeMongoImpl, "connect")
    async def test_success_startup(self, mock_connect):
        """Should call connect and log success"""

        with capture_logs() as logs:
            await self._mongo.process_startup({}, {})

        self.assertEqual(len(logs), 1)
        self.assert_log(logs[0], "SUCCESS", "Connected to MongoDB")

        mock_connect.assert_awaited_once()

    @mock.patch.object(FakeMongoImpl, "connect")
    async def test_connect_fail_on_startup(self, mock_connect):
        """Should catch an error and log it"""
        mock_connect.side_effect = Exception("Actual error")

        with capture_logs() as logs:
            await self._mongo.process_startup({}, {})

        self.assertEqual(len(logs), 2)
        self.assert_log(logs[0], "ERROR", "Failed connection to MongoDB")
        self.assert_log(logs[1], "ERROR", "Actual error")

        mock_connect.assert_awaited_once()

    @mock.patch.object(FakeMongoImpl, "disconnect")
    async def test_success_shutdown(self, mock_disconnect):
        """Should call disconnect and log success"""

        with capture_logs() as logs:
            await self._mongo.process_shutdown({}, {})

        self.assertEqual(len(logs), 1)
        self.assert_log(logs[0], "SUCCESS", "Disconnected from MongoDB")

        mock_disconnect.assert_awaited_once()

    @mock.patch.object(FakeMongoImpl, "disconnect")
    async def test_disconnect_fail_on_shutdown(self, mock_disconnect):
        """Should catch an error and log it"""
        mock_disconnect.side_effect = Exception("Actual error")

        with capture_logs() as logs:
            await self._mongo.process_shutdown({}, {})

        self.assertEqual(len(logs), 2)
        self.assert_log(logs[0], "ERROR", "Failed disconnection from MongoDB")
        self.assert_log(logs[1], "ERROR", "Actual error")

        mock_disconnect.assert_awaited_once()
