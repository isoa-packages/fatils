import datetime
import re
from unittest import TestCase
from zoneinfo import ZoneInfo

from fatils import UTC
from fatils import iso_utc_datetime
from fatils import utc_datetime
from fatils.helpers.datetime import IANA_UTC_TIMEZONE


# Pattern to match "2015-04-25T06:30:15.123456+01:00"
ISO_UTC_PATTERN = re.compile(
    "^(?:(?P<date>\d{4}-\d{2}-\d{2})T(?P<time>(?P<hour>[0-1][0-9]|2[0-3]):(?P<minute>[0-5][0-9]):(?P<second>[0-5][0-9]).\d+?)\+(?P<tz>[0-1][0-9]:[0-5][0-9]))$"
)


class TestDatetime(TestCase):
    def test_utc(self):
        now = utc_datetime()
        self.assertTrue(isinstance(now, datetime.datetime))
        self.assertTrue(isinstance(now.tzinfo, ZoneInfo))
        self.assertTrue(isinstance(UTC, ZoneInfo))
        self.assertEqual(now.tzinfo, UTC)
        self.assertEqual(now.tzinfo.key, IANA_UTC_TIMEZONE)

    def test_iso_utc(self):
        now = iso_utc_datetime()
        self.assertTrue(isinstance(now, str))
        self.assertTrue(re.match(ISO_UTC_PATTERN, now))
