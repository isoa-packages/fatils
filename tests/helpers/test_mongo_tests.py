from unittest import IsolatedAsyncioTestCase
from unittest import mock

from fatils import BaseMongoTest


MONGO_TEST_PATH = "fatils.helpers.mongo_tests"


class FakeMongoImpl:
    def connect(self): ...  # pragma: no cover

    def disconnect(self): ...  # pragma: no cover


class FakeFixturesImpl:
    def insert(self): ...  # pragma: no cover

    def delete(self): ...  # pragma: no cover


class TestBaseMongoTest(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        self.test = BaseMongoTest()

        self.fake_mongo_impl = FakeMongoImpl()
        self.fake_fixtures_impl = FakeFixturesImpl()

    @mock.patch(f"{MONGO_TEST_PATH}.MongoMiddleware.process_startup")
    @mock.patch(f"{MONGO_TEST_PATH}.FixturesMiddleware.process_startup")
    @mock.patch(f"{MONGO_TEST_PATH}.MongoMiddleware.process_shutdown")
    @mock.patch(f"{MONGO_TEST_PATH}.FixturesMiddleware.process_shutdown")
    async def test_start_and_shutdown_with_fixtures(
        self,
        mock_fixtures_shut,
        mock_mongo_shut,
        mock_fixtures_start,
        mock_mongo_start,
    ):
        """Should call startup and shutdown on middlewares of mongo and fixtures"""
        await self.test.asyncSetUp(
            mongo=self.fake_mongo_impl, fixtures=self.fake_fixtures_impl
        )
        mock_mongo_start.assert_awaited_once()
        mock_fixtures_start.assert_awaited_once()

        await self.test.asyncTearDown()
        mock_mongo_shut.assert_awaited_once()
        mock_fixtures_shut.assert_awaited_once()

    @mock.patch(f"{MONGO_TEST_PATH}.MongoMiddleware.process_startup")
    @mock.patch(f"{MONGO_TEST_PATH}.FixturesMiddleware.process_startup")
    @mock.patch(f"{MONGO_TEST_PATH}.MongoMiddleware.process_shutdown")
    @mock.patch(f"{MONGO_TEST_PATH}.FixturesMiddleware.process_shutdown")
    async def test_start_and_shutdown_without_fixtures(
        self,
        mock_fixtures_shut,
        mock_mongo_shut,
        mock_fixtures_start,
        mock_mongo_start,
    ):
        """Should call startup and shutdown on mongo only"""
        await self.test.asyncSetUp(mongo=self.fake_mongo_impl)

        mock_mongo_start.assert_awaited_once()
        mock_fixtures_start.assert_not_awaited()

        await self.test.asyncTearDown()

        mock_mongo_shut.assert_awaited_once()
        mock_fixtures_shut.assert_not_awaited()
