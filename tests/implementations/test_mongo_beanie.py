from unittest import IsolatedAsyncioTestCase
from unittest import mock

from beanie import Document
from pydantic import SecretStr

from fatils.implementations import MongoBeanie


PATCH_MONGO_BEANIE = "fatils.implementations.mongo_beanie"


class BaseBeanieDocument(Document):
    name: str


class FakeAsyncMotorClient:
    def get_database(self, db_name):
        return db_name

    def close(self): ...  # pragma: no cover


class TestMongoBeanie(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._async_motor_client = FakeAsyncMotorClient()
        self._beanie = MongoBeanie(
            [BaseBeanieDocument],
            SecretStr("secret_uri"),
            "database_name",
        )

    @mock.patch(f"{PATCH_MONGO_BEANIE}.AsyncIOMotorClient")
    @mock.patch(f"{PATCH_MONGO_BEANIE}.init_beanie")
    async def test_success_connect(self, mock_beanie, mock_async_motor):
        """Should call AsyncIOMotorClient and init_beanie, once each"""
        mock_async_motor.return_value = self._async_motor_client

        await self._beanie.connect()

        mock_async_motor.assert_called_once_with("secret_uri")
        mock_beanie.assert_awaited_once_with(
            database="database_name", document_models=[BaseBeanieDocument]
        )

    @mock.patch(f"{PATCH_MONGO_BEANIE}.AsyncIOMotorClient")
    @mock.patch(f"{PATCH_MONGO_BEANIE}.init_beanie")
    async def test_fail_connect_error_on_motor(
        self, mock_beanie, mock_async_motor
    ):
        """Should raise an error on AsyncIOMotorClient and not call init_beanie"""
        mock_async_motor.side_effect = Exception("Actual error")

        with self.assertRaises(Exception):
            await self._beanie.connect()

        mock_async_motor.assert_called_once_with("secret_uri")
        mock_beanie.assert_not_awaited()

    @mock.patch(f"{PATCH_MONGO_BEANIE}.AsyncIOMotorClient")
    @mock.patch(f"{PATCH_MONGO_BEANIE}.init_beanie")
    async def test_fail_connect_error_on_init(
        self, mock_beanie, mock_async_motor
    ):
        """Should call AsyncIOMotorClient and raise an error on init_beanie"""
        mock_async_motor.return_value = self._async_motor_client
        mock_beanie.side_effect = Exception("Actual error")

        with self.assertRaises(Exception):
            await self._beanie.connect()

        mock_async_motor.assert_called_once_with("secret_uri")
        mock_beanie.assert_awaited_once_with(
            database="database_name", document_models=[BaseBeanieDocument]
        )

    @mock.patch.object(FakeAsyncMotorClient, "close")
    async def test_success_disconnect_with_client(self, mock_close):
        """Should call close on client and set it to None"""
        self._beanie._client = self._async_motor_client

        await self._beanie.disconnect()

        mock_close.assert_called_once()
        self.assertIsNone(self._beanie._client)

    @mock.patch.object(FakeAsyncMotorClient, "close")
    async def test_success_disconnect_without_client(self, mock_close):
        """Should do nothing"""

        await self._beanie.disconnect()

        mock_close.assert_not_called()
        self.assertIsNone(self._beanie._client)
