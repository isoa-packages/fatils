from unittest import IsolatedAsyncioTestCase
from unittest import mock

from beanie import Document
from beanie.exceptions import CollectionWasNotInitialized
from pydantic import BaseModel

from fatils.implementations import BeanieDocumentFixture
from fatils.implementations import FixturesBeanie
from fatils.implementations.fixtures_beanie import _insert_function


PATCH_FIXTURES_JSON = "tests/fixtures"


class BaseBeanieDocument(Document):
    name: str


class FakeBeanieDocument(BaseModel):
    name: str

    async def insert_many(self, documents): ...  # pragma: no cover

    async def delete_all(self): ...  # pragma: no cover


class TestFixturesBeanie(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.mock_insert_func = mock.AsyncMock()
        fixture_one = BeanieDocumentFixture(
            f"{PATCH_FIXTURES_JSON}/beanie_one.json",
            FakeBeanieDocument,
            self.mock_insert_func,
        )
        fixture_two = BeanieDocumentFixture(
            f"{PATCH_FIXTURES_JSON}/beanie_two.json",
            FakeBeanieDocument,
            self.mock_insert_func,
        )

        _fixtures = [fixture_one, fixture_two]

        self._beanie = FixturesBeanie(_fixtures)

    @mock.patch.object(FakeBeanieDocument, "insert_many")
    async def test_success_default_insert_function(
        self, mock_insert_many
    ) -> None:
        """Should read file and insert documents"""
        await _insert_function(
            f"{PATCH_FIXTURES_JSON}/beanie_one.json", FakeBeanieDocument
        )
        expected_documents = [
            FakeBeanieDocument(**entry)
            for entry in [
                {"name": "first_beanie_one.json"},
                {"name": "second_beanie_one.json"},
            ]
        ]
        mock_insert_many.assert_awaited_once()
        mock_insert_many.assert_awaited_with(expected_documents)

    @mock.patch.object(FakeBeanieDocument, "insert_many")
    async def test_fail_default_insert_function(
        self, mock_insert_many
    ) -> None:
        """Should read file and raise error on document not intiated"""
        with self.assertRaises(CollectionWasNotInitialized):
            await _insert_function(
                f"{PATCH_FIXTURES_JSON}/beanie_one.json", BaseBeanieDocument
            )

        mock_insert_many.assert_not_awaited()

    async def test_success_insert(self) -> None:
        """Should insert all documents"""
        await self._beanie.insert()

        self.assertEqual(self.mock_insert_func.await_count, 2)
        self.mock_insert_func.assert_has_awaits(
            [
                mock.call(
                    f"{PATCH_FIXTURES_JSON}/beanie_one.json",
                    FakeBeanieDocument,
                ),
                mock.call(
                    f"{PATCH_FIXTURES_JSON}/beanie_two.json",
                    FakeBeanieDocument,
                ),
            ]
        )

    @mock.patch.object(FakeBeanieDocument, "delete_all")
    async def test_success_delete(self, mock_delete) -> None:
        """Should delete all documents"""
        await self._beanie.delete()

        self.assertEqual(mock_delete.await_count, 2)
