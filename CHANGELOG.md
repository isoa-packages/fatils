## 1.2.0 (2024-06-19)

### Feat

- shorthand for utc zoneinfo

## 1.1.0 (2024-06-18)

### Feat

- utc datetime helper

## 1.0.0 (2024-06-16)

### Feat

- falcon app, helpers, middlewares and implementations
